# -*- coding: utf-8 -*-
"""
Created on Thu Apr 22 20:46:16 2021

@author: ibran
"""


# -*- coding: utf-8 -*-
"""
Created on Wed Apr  7 21:22:29 2021

@author: ibran
"""


import pandas as pd
import pandasql as ps
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.preprocessing import OrdinalEncoder

import os 
os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = 'C:/Users/Bianca/AppData/Local/r-miniconda/envs/r-reticulate/Library/plugins/platforms'


#read the list of countries
countries_df_py = pd.read_csv("C:/Users/Bianca/Desktop/FMI/Data Visualization/Countries.csv")

#read dataset
df_hotels = pd.read_csv("C:/Users/Bianca/Desktop/FMI/Data Visualization/Hotel_Reviews.csv")

#drop duplicates
df_hotels = df_hotels.drop_duplicates()

#split the data 
d = {k: set(v.split(' ')) for k, v in df_hotels['Hotel_Address'].items()}

#create new column
df_hotels['Hotel_Country'] = pd.Series(' ', index = df_hotels.index)

#assign the countries to the new column
for x in countries_df_py['Country_Name'] :
    for k,v in d.items():
        if(set(x.split(' ')).issubset(v)):
            df_hotels.at[k,'Hotel_Country'] = x
#df.to_csv('C:/Users/ibran/Desktop/Hotel_Reviews_countries.csv')
q1 = "SELECT COUNT(DISTINCT Hotel_Address) as no_of_hotels, Hotel_Country FROM df_hotels GROUP BY Hotel_Country"
q1_dataframe = ps.sqldf(q1,locals())


# Encode the "Hotel_Country" and "Nationality" columns into numerical data:
hotel_country = df_hotels.Hotel_Country.unique()
reviewer_nationalities = df_hotels.Reviewer_Nationality.unique()

encoder = OrdinalEncoder(dtype=np.float64)

# Encode countries
encoder.fit(hotel_country[:, np.newaxis])
hotel_country_encoded = encoder.transform(df_hotels.Hotel_Country[:, np.newaxis]).ravel()
hotel_country_unique = np.unique(hotel_country_encoded)

# Encode nationalities
encoder.fit(reviewer_nationalities[:, np.newaxis])
reviewer_nationalities_encoded = encoder.transform(df_hotels.Reviewer_Nationality[:, np.newaxis]).ravel()
reviewer_nationalities_unique = np.unique(reviewer_nationalities_encoded)

#Encode Hotel Name
hotel_name = df_hotels.Hotel_Name.unique()
encoder.fit(hotel_name[:, np.newaxis])
hotel_name_encoded = encoder.transform(df_hotels.Hotel_Name[:, np.newaxis]).ravel()
hotel_name_unique = np.unique(hotel_name_encoded)

#list of headers
#list(df_hotels.columns)

# Now aggregate all the encoded data in a single dataframe. This will allow for
# very fast indexing and array operations.
data_encoded = pd.DataFrame({
    #"Hotel_Address": df_hotels.Hotel_Address,
    "Additional_Number_of_Scoring": df_hotels.Additional_Number_of_Scoring,
    #"Review_Date": df_hotels.Review_Date,
    "Average_Score": df_hotels.Average_Score,
    "Hotel_Name": hotel_name_encoded,
    "Reviewer_Nationality": reviewer_nationalities_encoded,
    "Negative_Review": df_hotels.Negative_Review,
    "Review_Total_Negative_Word_Counts": df_hotels.Review_Total_Negative_Word_Counts,
    "Total_Number_of_Reviews": df_hotels.Total_Number_of_Reviews,
    "Positive_Review": df_hotels.Positive_Review,
    "Review_Total_Positive_Word_Counts": df_hotels.Review_Total_Positive_Word_Counts,
    "Total_Number_of_Reviews_Reviewer_Has_Given": df_hotels.Total_Number_of_Reviews_Reviewer_Has_Given,
    "Reviewer_Score": df_hotels.Reviewer_Score,
    #"Tags": df_hotels.Tags,
    #"days_since_review": df_hotels.days_since_review,
    "lat": df_hotels.lat,
    "lng": df_hotels.lng,
    "Hotel_Country": hotel_country_encoded
})

#Correlation between the variables  - moved in r 
#df_hotels_corr = df_hotels.corr()
#plt.figure(figsize=(15,10))
#sns.heatmap(df_hotels_corr, annot = True)
#plt.title("Correlation between the variables", fontsize = 22)

#Correlation between columns
#data_encoded_corr = data_encoded.corr()
#mask = np.zeros_like(data_encoded_corr)
#mask[np.triu_indices_from(mask)] = True
#with sns.axes_style("white"):
#   ax = sns.heatmap(data_encoded_corr, mask=mask, vmax=.3, square=True,  cmap="YlGnBu")
#   plt.show()
    
# Preparing for using SentimentIntensityAnalyzer 
# Convert the reviews to lower and delete leading/trailing space
data_encoded["Negative_Review"] = data_encoded["Negative_Review"].str.lower().str.strip()
data_encoded["Positive_Review"] = data_encoded["Positive_Review"].str.lower().str.strip()


from nltk.sentiment.vader import SentimentIntensityAnalyzer
sent_analyzer = SentimentIntensityAnalyzer()

#test using two reviews from the dataset
first_review = "The place is beautiful and really well presented Great food and really good staff"
second_review = "Room was really small, the worst ever."

print(f"review 1:\n{first_review}\nScore: {sent_analyzer.polarity_scores(first_review)}")

print(f"\nreview 2:\n{second_review}\nScore: {sent_analyzer.polarity_scores(second_review)}")

# The compound is the general positivity of a text.
# Above 0, it is positive and under 0, it is negative.
# This can be used to determine if a text is positive or negative.

#Creating two new columns - one for the positive reviews and the other for the negative ones
import time 
start_time = time.time()
positive = data_encoded["Positive_Review"].apply(lambda x: abs(sent_analyzer.polarity_scores(x)["compound"]))
negative = data_encoded["Negative_Review"].apply(lambda x: -abs(sent_analyzer.polarity_scores(x)["compound"]))

data_encoded["Sentiment_Score"] = positive + negative
data_encoded["Polarity_Positive"] = positive
data_encoded["Polarity_Negative"] = negative

time_model = time.time() - start_time
print(f"Execution time: {int(time_model)} seconds")

df_corr=data_encoded.corr()
# Get the colors for the graphic
colors = []
dim = df_corr.shape[0]
for i in range(dim):
    r = i * 1/dim
    colors.append((0.4,r,0.7))
    
# Transform each value in a positive value, because what interesses us
# isn't the direction of the correlation but the absolute correlation
#df_corr["Reviewer_Score"].apply(lambda x: abs(x)).sort_values().plot.barh(color = colors)
#plt.title("Correlation with Reviewer_Score", fontsize = 16)
#plt.show()

# Columns to use to train the models
# Get the columns with the highest correlation 
#cols = ['Review_Total_Negative_Word_Counts',
#        'Average_Score',
#        'Polarity_Positive',
#        'Sentiment_Score',
#        'Review_Total_Positive_Word_Counts']

cols = ['Reviewer_Score',
        'Review_Total_Negative_Word_Counts',
        'Average_Score',
        'Polarity_Positive',
        'Sentiment_Score',
        'Review_Total_Positive_Word_Counts']
        
X = data_encoded[cols].values
y = data_encoded["Reviewer_Score"].values

# Use StandardScaler to scale the data
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X = scaler.fit_transform(X)

#from sklearn.model_selection import train_test_split
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

