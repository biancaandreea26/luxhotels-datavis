# LuxuriousHotels
University Related 

Information Visualization project 

Sketch Project Contents: 
In the first part we will do some data analysis:
1. Search for duplicates and exclude them (if found)
2. Check for the missing values in the dataset
3. Complete the missing data based on other lines
(more steps to come after doing some EDA) 

After that, for the EDA:
1. Finding the nationalities with the highest scores - meaning who gives the best scores to hotels
2. Finding the nationalities with the lowest scores - meaning who gives the worst scores 
3. Number of reviews given by each nationality
4. Top ten reviewers based on the nationality and no of reviwes.
5. Distribution of scores : No of hotels by average scores
6. Density of Reviewer's Scores by hotels in the United Kingdom
7. Distribution of Countries by the number of Hotels - most likely we should create a new column with the country names by using the hotel address columns
8. Top ten the most positive reviewed Hotels
9. Top ten the most negative reviewed Hotels
10. Classification based on the Date column - Spring/Summer etc (no of reviews) - using the date column and creating 4 categories : spring,summer,winter,autumn - most likely we should create a new column 
11. No of Hotels by average no of reviews - a piechart and a 3D piechart
12. The most used words in positive reviews
13. The most used words in negtive reviews
14. The most used words in reviews(in general) - based on a random selection
15. Correlation Matrix
16. MAP: The location of hotels
17. Histogram : Creating a histogram with 3 categories of hotels based on ranking scores - below average, average and high
18. Correlation between variables using ScatterPlot Matrix
19. Time series visualization - No of Reviews/Day


MODELLING DATA
1. PCA - trying to use Countries/Reviewer_Nationality for  data grouping ;
2. TSNE - trying all the columns as well as trying to use Countries/Reviewer_Nationality for  data grouping  or getting only a sample of random 30000 lines from dataset
3. Sentiment Analysis - a python analysis based on Negative_Review and Positive_Review which gave us a starting point for predicting the Reviewer Score in the regression modelling part. We created two new columns: Polarity_Score and Sentiment_Score.
4. Linear Regression - short description, model, explanation of summary, predictions and conclusions
5. kNN Regression - short description, model, predictions, plot containing the predictions
6. Gradient Boosting Regression - short description, model, predictions, conclusions
7. XGBoost - short description, model, predictions and conclusions

INTERPRETING BEST PERFORMING MODEL - XGBoost
1. LIME - explain function, plotting the features and their influence when precdicting the Reviwer_Score
2. SHAP - importance of the features, two dependence plots: one for the most important feature and one for the least. 
